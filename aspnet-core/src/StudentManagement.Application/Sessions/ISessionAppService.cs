﻿using System.Threading.Tasks;
using Abp.Application.Services;
using StudentManagement.Sessions.Dto;

namespace StudentManagement.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
