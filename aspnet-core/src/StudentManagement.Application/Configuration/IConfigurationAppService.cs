﻿using System.Threading.Tasks;
using StudentManagement.Configuration.Dto;

namespace StudentManagement.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
