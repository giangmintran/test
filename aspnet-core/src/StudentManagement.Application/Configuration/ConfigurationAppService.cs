﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using StudentManagement.Configuration.Dto;

namespace StudentManagement.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : StudentManagementAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
