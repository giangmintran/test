﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Paged
{
    public class PagedRequestDtoBase : PagedResultRequestDto
    {
        private string _keyword;

        public string Keyword
        {
            get => _keyword;
            set => _keyword = value?.Trim();
        }
    }
}

