﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using StudentManagement.Authorization;

namespace StudentManagement
{
    [DependsOn(
        typeof(StudentManagementCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class StudentManagementApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<StudentManagementAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(StudentManagementApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
