﻿using Abp.AutoMapper;
using StudentManagement.Entities;
using StudentManagement.Services.StudentServices.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Services.ClassService.Dto
{
    [AutoMapTo(typeof(ClassReadModel))]
    public class CreateClassDto
    {
        [Required(ErrorMessage = "Tên lớp không được để trống")]
        public string ClassName { get; set; }
        public ICollection<StudentDto> Students { get; set; }
    }
}
