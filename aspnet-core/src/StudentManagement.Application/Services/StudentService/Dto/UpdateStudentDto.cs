﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using StudentManagement.Entities;
using StudentManagement.Services.StudentServices.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Services.StudentService.Dto
{
    [AutoMapTo(typeof(StudentReadModel))]
    public class UpdateStudentDto : EntityDto<int>
    {
        [Required(ErrorMessage = "Tên không được bỏ trống")]
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public bool Gender { get; set; }
    }
}
