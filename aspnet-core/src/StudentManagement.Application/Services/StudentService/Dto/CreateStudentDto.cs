﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using StudentManagement.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Services.StudentServices.Dto
{
    [AutoMapTo(typeof(StudentReadModel))]
    public class CreateStudentDto 
    {
        [Required(ErrorMessage = "Tên sinh viên không được bỏ trống")]
        public string Name { get; set; }
        public string Address { get; set; }
        public bool Gender { get; set; }
        public int Age { get; set; }
    }
}
