﻿using Abp.AutoMapper;
using StudentManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.Services.StudentServices.Dto
{
    [AutoMapFrom(typeof(StudentReadModel))]
    public class StudentDto
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public bool Gender { get; set; }
    }
}
