﻿using Abp.Application.Services.Dto;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using StudentManagement.Entities;
using StudentManagement.EntityFrameworkCore.Repositories;
using StudentManagement.Services.PagedService;
using StudentManagement.Services.StudentService.Dto;
using StudentManagement.Services.StudentServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StudentManagement.Services.StudentServices
{
    public class StudentService : StudentManagementAppServiceBase
    {
        private readonly StudentRepository _studentRepo;
        private readonly IUnitOfWorkManager _unitOfWork;

        public StudentService(StudentRepository studentRepo, IUnitOfWorkManager unitOfWork)
        {
            _studentRepo = studentRepo;
            _unitOfWork = unitOfWork;
        }

        public async Task<StudentReadModel> CreateAsync(CreateStudentDto viewModel)
        {
            
            viewModel.Name = viewModel.Name?.Trim();
            viewModel.Address = viewModel.Address?.Trim();
            Regex regex = new Regex("\\d+");

            if (regex.IsMatch(viewModel.Name))
            {
                throw new ArgumentException("Tên không hợp lệ");
            }
            var unitOfWork = _unitOfWork.Begin();
            var createStudent = ObjectMapper.Map<StudentReadModel>(viewModel);
            _studentRepo.Insert(createStudent);
            CurrentUnitOfWork.SaveChanges();
            unitOfWork.Complete();
            return createStudent;
        }

        public async Task<StudentReadModel> UpdateAsync(UpdateStudentDto viewModel)
        {
            var student = _studentRepo.FirstOrDefault(entity => entity.Id == viewModel.Id && !entity.IsDeleted);
            if(student == null)
            {
                throw new ArgumentException("Sinh viên không tồn tại");
            }

            viewModel.Name = viewModel.Name?.Trim();
            viewModel.Address = viewModel.Address?.Trim();

            Regex regex = new Regex("\\d+");

            if (regex.IsMatch(viewModel.Name))
            {
                throw new ArgumentException("Tên không hợp lệ");
            }
            var unitOfWork = _unitOfWork.Begin();
            var updateStudent = ObjectMapper.Map<StudentReadModel>(viewModel);
            _studentRepo.Update(updateStudent);
            CurrentUnitOfWork.SaveChanges();
            unitOfWork.Complete();  
            return updateStudent;
        }
        public async Task<StudentReadModel> DeleteAsync(int id)
        {
            var student = _studentRepo.FirstOrDefault(entity => entity.Id == id && !entity.IsDeleted);
            if (student == null)
            {
                throw new ArgumentException("Sinh viên không tồn tại");
            }
            student.IsDeleted = true;
            CurrentUnitOfWork.SaveChanges();
            return student;
        }

        
        public async Task<PagedResultDto<StudentDto>> GetAllAsync(PagedServices input)
        {
            var result = new PagedResultDto<StudentDto>();
            var query = _studentRepo.GetDbQueryTable().AsQueryable().Where(entity => !entity.IsDeleted);

            if (!string.IsNullOrEmpty(input.Keyword))
            {
                query = query.Where(o => o.Name.Contains(input.Keyword));
            }

            var items = await query.OrderByDescending(o => o.Id)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToListAsync();
            var studentCollection = ObjectMapper.Map<List<StudentDto>>(items);

            result.Items = studentCollection;
            result.TotalCount = await query.CountAsync();
            return result;
        }
        public async Task<ICollection<StudentDto>> GetAllStudentAsync()
        {
           
            var query = _studentRepo.GetDbQueryTable().AsQueryable().Where(entity => !entity.IsDeleted);
            var studentCollection = ObjectMapper.Map<ICollection<StudentDto>>(query);
            
            return studentCollection;
        }
        public async Task<StudentDto> GetById(int id)
        {
            var student = _studentRepo.FirstOrDefault(entity => entity.Id == id);
            if (student == null)
            {
                throw new ArgumentException("Không tìm thấy sinh viên");
            }
            

            return ObjectMapper.Map<StudentDto>(student);
        }

    }
}
