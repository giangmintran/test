﻿using Abp.Application.Services;
using StudentManagement.MultiTenancy.Dto;

namespace StudentManagement.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

