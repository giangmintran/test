using System.ComponentModel.DataAnnotations;

namespace StudentManagement.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}