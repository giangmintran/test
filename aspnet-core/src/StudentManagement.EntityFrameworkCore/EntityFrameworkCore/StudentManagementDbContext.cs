﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using StudentManagement.Authorization.Roles;
using StudentManagement.Authorization.Users;
using StudentManagement.MultiTenancy;
using StudentManagement.Entities;

namespace StudentManagement.EntityFrameworkCore
{
    public class StudentManagementDbContext : AbpZeroDbContext<Tenant, Role, User, StudentManagementDbContext>
    {
        /* Define a DbSet for each entity of the application */

        public StudentManagementDbContext(DbContextOptions<StudentManagementDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<StudentReadModel>().HasKey(entity => entity.Id);
            modelBuilder.Entity<StudentReadModel>(entity =>
            {
                entity.ToTable("Student");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .HasColumnType("int")
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.Name)
                    .HasColumnType("nvarchar(max)")
                    .IsRequired();
                entity.Property(e => e.Age)
                    .HasColumnType("int")
                    .IsRequired();

                entity.Property(e => e.Address)
                    .HasColumnType("nvarchar(max)");
                entity.Property(e => e.IsDeleted)
                    .IsRequired();
            });
            modelBuilder.Entity<ClassReadModel>().HasKey(entity => entity.Id);
            modelBuilder.Entity<ClassReadModel>(entity =>
            {
                entity.ToTable("Classes");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .HasColumnType("int")
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.ClassName)
                    .HasColumnType("nvarchar(max)")
                    .IsRequired();
                entity.Property(e => e.IsDeleted)
                    .IsRequired();

            });
            modelBuilder.Entity<CourseReadModel>().HasKey(entity => entity.Id);
            modelBuilder.Entity<CourseReadModel>(entity =>
            {
                entity.ToTable("Course");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .HasColumnType("int")
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.CourseName)
                    .HasColumnType("nvarchar(max)")
                    .IsRequired();
                entity.Property(e => e.Description)
                    .HasColumnType("nvarchar(max)")
                    .IsRequired();
                entity.Property(e => e.IsDeleted)
                    .IsRequired();
            });
            modelBuilder.Entity<ClassStudentReadModel>().HasKey(entity => entity.Id);
            modelBuilder.Entity<ClassStudentReadModel>(entity =>
            {
                entity.ToTable("ClassStudent");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .HasColumnType("int")
                    .ValueGeneratedOnAdd()
                    .IsRequired();
                entity.Property(e => e.StudentId)
                    .HasColumnType("int")
                    .IsRequired();
                entity.Property(e => e.ClassId)
                    .HasColumnType("int")
                    .IsRequired();
                entity.Property(e => e.IsDeleted)
                    .IsRequired();
            });
            modelBuilder.Entity<ClassStudentReadModel>().HasIndex(entity => entity.StudentId);
            modelBuilder.Entity<ClassStudentReadModel>().HasIndex(entity => entity.ClassId);

        }
    }
}
