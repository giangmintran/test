using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace StudentManagement.EntityFrameworkCore
{
    public static class StudentManagementDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<StudentManagementDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<StudentManagementDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
