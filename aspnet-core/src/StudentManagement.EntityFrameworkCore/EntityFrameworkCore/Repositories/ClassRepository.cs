﻿using Abp.EntityFrameworkCore;
using StudentManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.EntityFrameworkCore.Repositories
{
    public class ClassRepository : StudentManagementRepositoryBase<ClassReadModel>
    {
        public ClassRepository(IDbContextProvider<StudentManagementDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
