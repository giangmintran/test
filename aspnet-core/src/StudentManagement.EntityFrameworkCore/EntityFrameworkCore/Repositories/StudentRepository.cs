﻿using Abp.EntityFrameworkCore;
using StudentManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.EntityFrameworkCore.Repositories
{
    public class StudentRepository : StudentManagementRepositoryBase<StudentReadModel>
    {
        public StudentRepository(IDbContextProvider<StudentManagementDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
