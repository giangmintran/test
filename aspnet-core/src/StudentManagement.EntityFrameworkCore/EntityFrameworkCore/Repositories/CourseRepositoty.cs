﻿using Abp.EntityFrameworkCore;
using StudentManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.EntityFrameworkCore.Repositories
{
    public class CourseRepository : StudentManagementRepositoryBase<CourseReadModel>
    {
        public CourseRepository(IDbContextProvider<StudentManagementDbContext> dbContextProvider) : base(dbContextProvider)
        {
        }
    }
}
