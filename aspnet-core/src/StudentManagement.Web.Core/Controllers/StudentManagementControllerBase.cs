using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace StudentManagement.Controllers
{
    public abstract class StudentManagementControllerBase: AbpController
    {
        protected StudentManagementControllerBase()
        {
            LocalizationSourceName = StudentManagementConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
