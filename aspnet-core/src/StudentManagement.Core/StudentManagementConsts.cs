﻿using StudentManagement.Debugging;

namespace StudentManagement
{
    public class StudentManagementConsts
    {
        public const string LocalizationSourceName = "StudentManagement";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;


        /// <summary>
        /// Default pass phrase for SimpleStringCipher decrypt/encrypt operations
        /// </summary>
        public static readonly string DefaultPassPhrase =
            DebugHelper.IsDebug ? "gsKxGZ012HLL3MI5" : "0fddbaea7d874df8916fc4a65713781b";
    }
}
