﻿using Abp.Authorization;
using StudentManagement.Authorization.Roles;
using StudentManagement.Authorization.Users;

namespace StudentManagement.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
