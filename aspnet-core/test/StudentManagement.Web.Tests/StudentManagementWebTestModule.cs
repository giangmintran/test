﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using StudentManagement.EntityFrameworkCore;
using StudentManagement.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace StudentManagement.Web.Tests
{
    [DependsOn(
        typeof(StudentManagementWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class StudentManagementWebTestModule : AbpModule
    {
        public StudentManagementWebTestModule(StudentManagementEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(StudentManagementWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(StudentManagementWebMvcModule).Assembly);
        }
    }
}