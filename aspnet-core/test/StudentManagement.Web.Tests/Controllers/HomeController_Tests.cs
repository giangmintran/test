﻿using System.Threading.Tasks;
using StudentManagement.Models.TokenAuth;
using StudentManagement.Web.Controllers;
using Shouldly;
using Xunit;

namespace StudentManagement.Web.Tests.Controllers
{
    public class HomeController_Tests: StudentManagementWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}