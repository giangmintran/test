import { StudentManagementTemplatePage } from './app.po';

describe('StudentManagement App', function() {
  let page: StudentManagementTemplatePage;

  beforeEach(() => {
    page = new StudentManagementTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
